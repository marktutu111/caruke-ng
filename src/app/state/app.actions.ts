import { AlertModel } from "../models/alert.model";
import { ExpenseCategoryPayload, ExpensePayload, ExpenseQueryType, ExpensesQuery } from "../models/expenses.model";
import { InvoiceModel, InvoicePayload } from "../models/invoice.model";
import { SalesModel } from "../models/sales.model";
import { UsersInterfaceModel } from "../models/users.model";
import { WasherModel } from "../models/washers.model";

export class PushWashers {
    static readonly type = '[appstate] PushWasher]';
    constructor(public readonly payload:Array<WasherModel>) {};
}


export class ShowAlert {
    static readonly type = '[appstate] ShowAlert]';
    constructor(public readonly payload:AlertModel) {};
}

export class SetState {
    static readonly type = '[appstate] SetState]';
    constructor(public readonly payload?:any) {}
}

export class GetOperations {
    static readonly type = '[appstate] GetOperations]';
    constructor(public readonly id?:string) {}
}

export class GetServices {
    static readonly type = '[appstate] GetServices]';
}


export class GetWashers {
    static readonly type = '[appstate] GetWashers]';
}


export class GetSales {
    static readonly type='[appstate] GetSales]';
    constructor(public readonly data?:any) {}
}

export class FilterSales {
    static readonly type='[appstate] FilterSales]';
    constructor(public readonly data:{start:string,end:string,washer?:string}) {}
}

export class GetSummary {
    static readonly type='[appstate] GetSummary]';
}

export class GetCustomers {
    static readonly type='[appstate] GetCustomers]';
}


export class SearchCustomers {
    static readonly type='[appstate] SearchCustomers]';
    constructor(public readonly search:string) {}
}


export class SelectSale {
    static readonly type='[appstate] SelectSale]';
    constructor(public readonly sale:SalesModel[]) {}
}


export class SelectInvoice {
    static readonly type='[appstate] SelectInvoice]';
    constructor(public readonly invoice:InvoiceModel) {}
}


export class GetUsers {
    static readonly type='[appstate] GetUsers]';
}

export class GetInvoices {
    static readonly type='[appstate] GetInvoices]';
}


export class AddInvoice {
    static readonly type='[appstate] AddInvoice]';
    constructor(public readonly invoice:InvoicePayload) {}

}


export class GetSubs {
    static readonly type='[appstate] GetSubs]';
    constructor(public readonly id?:string) {}
}


export class FilterExpenses {
    static readonly type='[appstate] FilterExpenses]';
    constructor(public readonly data:ExpenseQueryType) {}
}

export class GetExpenseCategory {
    static readonly type='[appstate] GetExpenseCategory]';
}

export class GetExpenses {
    static readonly type='[appstate] GetExpenses]';
}



export class GetBays {
    static readonly type='[appstate] GetBays]';
}


export class GetSettings {
    static readonly type='[appstate] GetSettings]';
}

export class GetUser {
    static readonly type='[appstate] GetUser]';
    constructor(public readonly id:string) {};
}


export class SetCurrentUser {
    static readonly type='[appstate] SetCurrentUser]';
    constructor(public data:UsersInterfaceModel|null,public rem?:boolean) {};
}


export class Logout {
    static readonly type='[appstate] Logout]';
}