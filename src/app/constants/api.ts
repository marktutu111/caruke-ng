import { environment } from "src/environments/environment";

let admin='';
let user='';
switch (environment.production) {
    case true:
        user='http://app.carukee.com/caruke';
        break;
    default:
        user='http://localhost:4040/caruke';
        break;
}

export default {
    admin,
    user
}