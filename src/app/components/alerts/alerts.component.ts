import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AlertModel } from 'src/app/models/alert.model';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-alerts',
    templateUrl: './alerts.component.html',
    styleUrls: ['./alerts.component.scss']
})
export class AlertComponent implements OnInit {

    @Select(AppState.alert) alert$:Observable<AlertModel>;

    constructor() {}

    ngOnInit(): void {}


}
