
export interface SettingsPayload {
    sms_message:string;
    sms_sales_toggle:boolean;
}


export interface Settings {
    _id:string;
    bay:string;
    sms_message:string;
    sms_sales_toggle:boolean;
    createdAt:string;
    updatedAt:string;
}


export class SettingsModel implements Settings {
    _id:string='';
    bay:string='';
    sms_message='';
    sms_sales_toggle=false;
    createdAt='';
    updatedAt='';
    constructor(setting:Settings){
        this._id=setting._id;
        this.bay=setting.bay;
        this.sms_message=setting.sms_message;
        this.sms_sales_toggle=setting.sms_sales_toggle;
        this.createdAt=setting.createdAt;
        this.updatedAt=setting.updatedAt;
    }
}