
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from "@ngxs/store";
import { AppState } from "./app.state";
import { WasherService } from '../services/washers.service';
import { SalesService } from '../services/sales.service';
import { ServicesService } from '../services/services.service';
import { CustomerService } from '../services/customer.service';
import { UserService } from '../services/users.service';
import { AdminService } from '../services/admin.service';
import { SettingsService } from '../services/settings.service';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { InvoiceService } from '../services/invoice.service';
import { ExpenseService } from '../services/expense.service';

@NgModule({
    imports: [ 
        CommonModule,
        NgxsRouterPluginModule.forRoot(),
        NgxsModule.forRoot([AppState])
    ],
    exports: [],
    providers: [
        WasherService,
        SalesService,
        ServicesService,
        CustomerService,
        UserService,
        AdminService,
        SettingsService,
        InvoiceService,
        ExpenseService
    ],
})
export class StateModule {}