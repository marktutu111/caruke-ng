import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import api from '../constants/api';
import { ExpensePayload, ExpensesQuery } from '../models/expenses.model';
import ResponseInterfaceModel from '../models/response.model';

@Injectable({
    providedIn: 'root'
})
export class ExpenseService {

    constructor(private http:HttpClient){}

    addExpense(data:ExpensePayload):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/expenses/add`,data)
            .toPromise()
            .then(res=>res)
            .catch(err=>err)
    }

    addExpenseCategory(payload:ExpensePayload):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/expenses/category`,payload)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    getExpenses():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/expenses/get`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    filterExpenses(query:string):Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/expenses/filter?${query}`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    getExpenseCategory():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/expenses/category/get`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    deleteExpenseCategory(id:string):Promise<ResponseInterfaceModel>{
        return this.http.delete(`${api.user}/expenses/category/delete/${id}`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    deleteExpense(id:string):Promise<ResponseInterfaceModel>{
        return this.http.delete(`${api.user}/expenses/delete/${id}`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }


}