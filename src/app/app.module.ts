import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EditSaleGuard } from './guards/editsale.guard';
import { HomeGuard } from './guards/home.guard';
import { InvoiceGuard } from './guards/invoice.guard';
import { RolesGuard } from './guards/roles.guard';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { StateModule } from './state/state.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StateModule,
    AppRoutingModule
  ],
  providers: [
    HomeGuard,
    RolesGuard,
    InvoiceGuard,
    EditSaleGuard,
    { provide: HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {};
