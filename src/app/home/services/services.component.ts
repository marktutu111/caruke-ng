import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { OperationsInterfaceModel, OperationsModel } from 'src/app/models/operations.model';
import ResponseInterfaceModel from 'src/app/models/response.model';
import { ServicesInterfaceModel, ServicesModel } from 'src/app/models/services.model';
import { ServicesService } from 'src/app/services/services.service';
import { GetOperations, GetServices, SetState, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

    @Select(AppState.services) services$:Observable<ServicesModel[]>;
    @Select(AppState.operations) operations$:Observable<OperationsModel[]>;
    loading:boolean=false;
    formGroup:FormGroup;
    s:number=1;
    p:number=1;

    private que:any[]=[];

    constructor(private service:ServicesService,private store:Store, private fb:FormBuilder) {}

    ngOnInit(): void {
        this.formGroup=this.fb.group(
            {
                services:new FormGroup(
                    {
                        name:new FormControl('',Validators.required),
                        commission:new FormControl(0,Validators.required)
                    }
                ),
                operations:new FormGroup(
                    {
                        service:new FormControl('',Validators.required),
                        amount:new FormControl('',Validators.required),
                        description:new FormControl('',Validators.required)
                    }
                )
            }
        );

        this.getData();

    }

    async queDelete(){
        try {
            if(this.que.length>0){
                this.loading=true;
                const _s:OperationsModel=this.que.shift();
                const response=await this.service.deleteOp(_s._id);
                if(!response.success){
                    throw Error(
                        response.message
                    )
                };
                this.loading=false;
                this.queDelete();
                this.getData();
                this.store.dispatch(
                    new ShowAlert(
                        {
                            show:true,
                            message:response.message,
                            type:'success'
                        }
                    )
                );
            }
        } catch (err:any) {
            this.queDelete();
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }

    delete(){
        try {
            this.que=this.store.selectSnapshot(AppState.operations).filter(op=>op.checked);
            this.que.length>0 && this.queDelete();
        } catch (err) {}
    }

    
    async getData(){
        this.store.dispatch(
            [
                new GetOperations(),
                new GetServices()
            ]
        )
    }

    async save(type:'s'|'o'){
        try {
            this.loading=true;
            let response!:ResponseInterfaceModel;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            switch (type) {
                case 's':
                    const service=this.formGroup.get('services');
                    if(!service?.valid){
                        throw Error(
                            'Please provide all required data'
                        )
                    }
                    response=await this.service.adds(
                        service.value
                    )
                    break;
                case 'o':
                    const operation=this.formGroup.get('operations');
                    if(!operation?.valid){
                        throw Error(
                            'Please provide all required data'
                        )
                    }
                    response=await this.service.addOperation(
                        operation.value
                    )
                    break;
                default:
                    break;
            };
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.formGroup.reset();
            this.getData();
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'success',
                        message:response.message,
                        show:true
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'danger',
                        message:err.message,
                        show:true
                    }
                )
            )
        }
    }


}
