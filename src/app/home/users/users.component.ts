import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InterfaceMenusItem, menuItems } from 'src/app/models/menus.model';
import ResponseInterfaceModel from 'src/app/models/response.model';
import { UserModel, UsersInterfaceModel, UsersPayloadInterface } from 'src/app/models/users.model';
import { UserService } from 'src/app/services/users.service';
import { GetUsers, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    @Select(AppState.users) users$:Observable<UserModel[]>;
    @Select(AppState.count) count$:Observable<string>;
    @Select(AppState.currentUser) currentUser$:Observable<UsersInterfaceModel>;
    @Select(AppState.loading) loading$:Observable<boolean>;

    menus:any=menuItems;
    loading:boolean=false;
    toggle:boolean=false;
    onEdit:boolean=false;
    p:number=1;

    formGroup:FormGroup=this.fb.group(
        {
           user:new FormGroup(
                {
                    _id:new FormControl(null),
                    fname:new FormControl('',Validators.required),
                    lname:new FormControl('',Validators.required),
                    email:new FormControl('',Validators.required),
                    password:new FormControl('',Validators.required),
                    phone:new FormControl('',Validators.required),
                    gender:new FormControl('',Validators.required),
                    role:new FormControl([],Validators.required),
                    status:new FormControl('',Validators.required),
                    account:new FormControl('super')
                }
            )
        }
    )

    constructor(private store:Store,private fb:FormBuilder,private service:UserService) {};

    


    ngOnInit(): void {
        this.store.dispatch(
            new GetUsers()
        )

    }

    reset(){
        this.formGroup.get('user').patchValue(
            {
                _id:null,
                fname:'',
                lname:'',
                email:'',
                password:'',
                phone:'',
                gender:'',
                role:[],
                status:'',
                account:'super'
            }
        )
    }

    async save(){
        try {
            if(!this.formGroup.get('user')?.valid){
                throw Error(
                    'Please provide all data'
                )
            };
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            this.loading=true;
            let response:ResponseInterfaceModel;
            const currentUser:UsersInterfaceModel|null=this.store.selectSnapshot(AppState.currentUser);
            const data:UserModel=this.formGroup.get('user')?.value;

            let _payload:UsersPayloadInterface={
                email:data.email,
                gender:data.gender,
                fname:data.fname,
                lname:data.lname,
                password:data.password,
                phone:data.phone,
                role:data.role,
                status:data.status
            };

            switch (currentUser?.account) {
                case 'bay':
                    _payload={..._payload,bay:currentUser.bay._id,account:'bay'};
                    break;
                default:
                    _payload={..._payload,account:data.account};
                    break;
            }

            switch (this.onEdit) {
                case true:
                    response=await this.service.update(
                        {
                            id:data._id,
                            data:_payload
                        }
                    );
                    break;
                default:
                    response=await this.service.add(_payload);
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.onEdit=false;
            this.toggle=false;
            this.reset();
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            show:true,
                            message:'User created successfully',
                            type:'success'
                        }
                    ),
                    new GetUsers()
                ]
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


    onChecked(_role:any,e:any){
        let {role}=this.formGroup.get('user')?.value;
        switch (e.checked) {
            case true:
                role=[...role,_role];
                break;
            default:
                role=role.filter((r:string)=>r!==_role);
                break;
        }
        this.formGroup.get('user')?.patchValue(
            {
                role:role
            }
        )
    }


    checked(role:any){
        return this.formGroup.get('user')?.value.role.indexOf(role)>-1;
    }



    edit(user:UserModel){
        this.toggle=true;
        this.onEdit=true;
        this.formGroup.get('user')?.patchValue(
            user
        )
    }


}
