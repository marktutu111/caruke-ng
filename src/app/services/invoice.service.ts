import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import api from '../constants/api';
import { InvoicePayload } from '../models/invoice.model';
import ResponseInterfaceModel from '../models/response.model';

@Injectable({
    providedIn: 'root'
})
export class InvoiceService {

    constructor(private http:HttpClient){}

    getInvoices():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/invoice/get`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    addInvoice(data:InvoicePayload):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/invoice/add`,data)
                        .toPromise()
                        .then(res=>res)
                        .catch(err=>err)
    }

    payInvoice(id:string):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/invoice/pay`,{ id:id })
                        .toPromise()
                        .then(res=>res)
                        .catch(err=>err)
    }

}