import { bay, BayInterface } from "./bay.model";


type accountType= 'bay' | 'super';


export interface UsersPayloadInterface {
    bay?:string;
    account?:string;
    fname:string;
    lname:string;
    email:string;
    password:string;
    phone:string;
    gender:string;
    role:string[];
    status:string;
}


export interface UsersInterfaceModel {
    bay:Partial<BayInterface>;
    account:accountType;
    subscriptionToken:string;
    daysRemaining?:string;
    _id:string;
    fname:string;
    lname:string;
    email:string;
    password:string;
    phone:string;
    gender:string;
    role:string[];
    status:string;
    createdAt:string;
    updatedAt:string;
    fullname?:()=>string;
    getBay?:(v:bay)=>string|any;
}


export class UserModel  implements UsersInterfaceModel {

    _id='';
    fname='';
    account:accountType='bay';
    subscriptionToken='';
    lname='';
    email='';
    password='';
    phone='';
    gender='';
    role;
    status='';
    createdAt='';
    updatedAt='';
    daysRemaining='';
    bay:any;

    constructor(user:UsersInterfaceModel){
        this._id=user._id;
        this.subscriptionToken=user.subscriptionToken;
        this.bay=user.bay;
        this.daysRemaining=user.daysRemaining;
        this.account=user.account;
        this.fname=user.fname;
        this.lname=user.lname;
        this.email=user.email;
        this.phone=user.phone;
        this.password=user.password;
        this.gender=user.gender;
        this.role=user.role;
        this.status=user.status;
        this.createdAt=user.createdAt;
        this.updatedAt=user.updatedAt;
    }

    fullname(){
        return `${this.fname} ${this.lname}`;
    }

    getBay(v:bay){
        try {
            return this.bay[v]   
        } catch (error) {
            return 'N/A';
        }
    }

}