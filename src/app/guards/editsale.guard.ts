import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SalesModel } from '../models/sales.model';
import { AppState } from '../state/app.state';

@Injectable({
    providedIn: 'root'
})
export class EditSaleGuard implements CanActivate {
    constructor(private store:Store,private router:Router){};
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const sale:SalesModel=this.store.selectSnapshot(AppState.selectedSale)[0];
        if(!sale || !sale._id){
            this.router.navigate(['app/sales']);
            return false;
        }
        return true;
    }
}
