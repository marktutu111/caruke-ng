import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import ResponseInterfaceModel from '../models/response.model';
import { SalesPayloadInterfaceModel } from '../models/sales.model';
import api from '../constants/api';
import { UpdatePayloadIntefaceModel } from '../models/common.model';

@Injectable({
    providedIn: 'root'
})
export class SalesService {

    constructor(private http:HttpClient){};

    getsales():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/sales/get`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    filter(data:any):Promise<ResponseInterfaceModel>{
        let queryString='';
        Object.keys(data).forEach((key)=>{
            if(key && data[key]!==''){
                queryString+=`${queryString.length>0?'&':''}${key}=${data[key]}`
            }
        });
        return this.http.get(`${api.user}/sales/filter?${queryString}`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    summary():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/sales/summary/get`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    addsale(data:SalesPayloadInterfaceModel):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/sales/add`,data)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    payInvoice(id:string):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/sales/invoice/pay`,{ id:id })
                        .toPromise()
                        .then(res=>res)
                        .catch(err=>err)
    }

    updatesale(data:UpdatePayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.put(`${api.user}/sales/update`,data)
                        .toPromise()
                        .then(res=>res)
                        .catch(err=>err)
    }


}