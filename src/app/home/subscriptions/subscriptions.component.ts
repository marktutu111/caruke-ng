import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SubscriptionModel } from 'src/app/models/subscription.model';
import { GetSubs } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-subscriptions',
    templateUrl: './subscriptions.component.html',
    styleUrls: ['./subscriptions.component.scss']
})
export class SubscriptionsComponent implements OnInit {

    @Select(AppState.subscriptions) subscriptions$:Observable<SubscriptionModel[]>;
    @Select(AppState.loading) loading$:Observable<boolean>;
    p:number=1;

    constructor(private store:Store) {};

    ngOnInit(): void {
        this.store.dispatch(
            new GetSubs()
        )
    }



}
