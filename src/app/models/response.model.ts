
export default interface ResponseInterfaceModel {
    success:boolean;
    message:string;
    data:any;
}