import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { UserModel, UsersInterfaceModel } from '../models/users.model';
import { UserService } from '../services/users.service';
import { SetCurrentUser } from '../state/app.actions';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    formGroup:FormGroup=new FormGroup(
        {
            email:new FormControl('',Validators.required),
            password:new FormControl('',Validators.required),
            rem:new FormControl(false)
        }
    )

    loading:boolean=false;
    alert:{ show:boolean,message?:string,type?:string }={show:false,message:'',type:''}

    constructor(private service:UserService,private store:Store,private router:Router) {};

    ngOnInit(): void {}


    async login(){
        try {
            this.alert={
                show:false
            }
            const form=this.formGroup;
            if(!form.valid){
                throw Error(
                    'Invalid form'
                )
            };
            this.loading=true;
            const {email,password,rem}=form.value;
            const response=await this.service.login(
                {
                    email:email,
                    password:password
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.store.dispatch(
                new SetCurrentUser(
                    new UserModel(response.data),
                    rem
                )
            );
            const user:UsersInterfaceModel=response.data;
            switch (user.account) {
                case 'bay':
                    this.router.navigate(['app/dashboard']);
                    break;
                default:
                    this.router.navigate(['app/users']);
                    break;
            }
        } catch (err:any) {
            this.loading=false;
            this.alert={
                message:err.message,
                show:true,
                type:'danger'
            }
        }
    }



}
