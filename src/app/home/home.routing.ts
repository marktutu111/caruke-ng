import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';

import { HomeComponent } from "./home.component";
import { ServicesComponent } from './services/services.component';
import { NewSaleComponent } from './sales/newsale/newsale.component';
import { SalesComponent } from './sales/sales.component';
import { UpdateSaleComponent } from './sales/updatesale/updatesale.component';
import { WashersComponent } from './washers/washers.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { UsersComponent } from './users/users.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { BayComponent } from './bay/bay.component';
import { ProfileComponent } from '../profile/profile.component';
import { RolesGuard } from '../guards/roles.guard';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoiceGuard } from '../guards/invoice.guard';
import { InvoiceListComponent } from './invoice/invoice-list.component';
import { InvoiceFormComponent } from './invoice/invoice-form.component';
import { EditSaleComponent } from './sales/edit-sale/edit-sale.component';
import { EditSaleGuard } from '../guards/editsale.guard';
import { ExpensesComponent } from './expenses/expenses.component';


const routes:Routes=[
    {
        path:'',
        redirectTo:'dashboard',
        pathMatch:'full'
    },
    { 
        path: '', 
        component:HomeComponent,
        children:[
            {
                path:'sales',
                canActivate:[RolesGuard],
                component:SalesComponent
            },
            {
                path:'addsale',
                canActivate:[RolesGuard],
                component:NewSaleComponent
            },
            {
                path:'editsale',
                canActivate:[RolesGuard,EditSaleGuard],
                component:EditSaleComponent
            },
            {
                path:'updatesale',
                canActivate:[RolesGuard],
                component:UpdateSaleComponent
            },
            {
                path:'services',
                canActivate:[RolesGuard],
                component:ServicesComponent
            },
            {
                path:'washers',
                canActivate:[RolesGuard],
                component:WashersComponent
            },
            {
                path:'subscriptions',
                canActivate:[RolesGuard],
                component:SubscriptionsComponent
            },
            {
                path:'customers',
                canActivate:[RolesGuard],
                component:CustomersComponent
            },
            {
                path:'dashboard',
                canActivate:[RolesGuard],
                component:DashboardComponent
            },
            {
                path:'users',
                canActivate:[RolesGuard],
                component:UsersComponent
            },
            {
                path:'subscribe',
                canActivate:[RolesGuard],
                component:SubscribeComponent
            },
            {
                path:'bay',
                canActivate:[RolesGuard],
                component:BayComponent
            },
            {
                path:'profile',
                canActivate:[RolesGuard],
                component:ProfileComponent
            },
            {
                path:'invoice',
                canActivate:[
                    RolesGuard,
                    InvoiceGuard
                ],
                component:InvoiceComponent
            },
            {
                path:'invoices',
                canActivate:[RolesGuard],
                component:InvoiceListComponent
            },
            {
                path:'newinvoice',
                canActivate:[RolesGuard],
                component:InvoiceFormComponent
            },
            {
                path:'expenses',
                component:ExpensesComponent
            }
        ]
    }
];

export default routes;
