import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InterfaceMenusItem, menuItems } from 'src/app/models/menus.model';
import { UserModel } from 'src/app/models/users.model';
import { Logout } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    @Select(AppState.currentUser) currentUser$:Observable<UserModel>;
    menus:InterfaceMenusItem=menuItems;

    constructor(private store:Store) {};

    ngOnInit(): void {};

    logout(){
        this.store.dispatch(
            new Logout()
        )
    }

    
}
