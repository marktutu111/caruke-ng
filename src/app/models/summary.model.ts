import { SalesModel } from "./sales.model";

export type SumTypes = '_id' 
    | 'profit' 
    | 'sales' 
    | 'commissions' 
    | 'count' 
    | 'items';


export interface SummaryItemsInteface{
    _id:string | string[];
    profit:number;
    sales:number;
    commissions:number;
    count:number;
    items:any[];
}


export interface SummaryInterfaceModel {
    today:SummaryItemsInteface[];
    week:SummaryItemsInteface[];
    month:SummaryItemsInteface[];
    year:SummaryItemsInteface[];
    services:SummaryItemsInteface[];
    all:SummaryItemsInteface[];
    paymentType:SummaryItemsInteface[];
    washers:any[];
    transactions:any[];
}


export class SummaryModel implements SummaryInterfaceModel {

    today;
    week;
    month;
    year;
    services;
    all;
    transactions:any=[];
    washers:any=[];
    paymentType:any=[];

    constructor(sum:SummaryInterfaceModel){
        this.today=sum.today;
        this.paymentType=sum.paymentType;
        this.week=sum.week;
        this.month=sum.month;
        this.year=sum.year;
        this.services=sum.services;
        this.all=sum.all;
        this.washers=sum.washers;
        this.transactions=sum.transactions;
    }

    getToday(v:SumTypes){
        return this.today.length>0 && this.today[0][v];
    }

    getAll(v:SumTypes){
        return this.all.length>0 && this.all[0][v]
    }

    calcPerc(service:string){
        const _tot=this.all[0].sales;
        const _sale=this.services.find(serv=>serv._id[0]===service)?.sales || 0;
        return ((100*_sale)/_tot).toFixed(0)+'%';
    }


    getSales():Array<SalesModel>{
        return this.transactions[0]?.items.map((s:any)=>new SalesModel(s));
    }

    getPaymentType(type:string){
        const sale=this.paymentType.find((p:any)=>{
            if(p._id===type){
                return p;
            }
        });
        return sale;
    }

    

}