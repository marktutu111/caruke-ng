import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InvoiceModel } from 'src/app/models/invoice.model';
import { UserModel } from 'src/app/models/users.model';
import { InvoiceService } from 'src/app/services/invoice.service';
import { ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-invoice',
    templateUrl: './invoice.component.html',
    styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

    @Select(AppState.currentUser) currentUser$:Observable<UserModel>;
    @Select(AppState.selectInvoice) selected$:Observable<InvoiceModel>;

    loading:boolean=false;

    constructor(private service:InvoiceService,private store:Store) {}

    ngOnInit(): void {};



    async pay(id:string){
        try {
            if(!id){
                return;
            };
            this.loading=true;
            const response=await this.service.payInvoice(id);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'success',
                        message:response.message,
                        show:true
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'error',
                        message:err.message,
                        show:true
                    }
                )
            )
        }
    }


    print(){
        window.print();
    }


}
