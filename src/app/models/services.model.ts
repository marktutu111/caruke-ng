
export interface ServicesPayloadIntefaceModel {
    name:string;
    commission:string;
}

export interface ServicesInterfaceModel {
    _id:string;
    name:string;
    image?:string;
    commission:string;
    status:string;
    createdAt:string;
    updatedAt:string;
}

export class ServicesModel implements ServicesInterfaceModel {

    _id='';
    name='';
    commission='';
    status='';
    createdAt='';
    updatedAt='';

    constructor(service:ServicesInterfaceModel){
        this._id=service._id;
        this.commission=service.commission;
        this.name=service.name;
        this.status=service.status;
        this.createdAt=service.createdAt;
        this.updatedAt=service.updatedAt;
    }

    getId(){
        return this._id.substring(0,10).toUpperCase();
    }

    getstatus(){
        return this.status==='blocked'?true:false;
    }

}