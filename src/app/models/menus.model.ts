
export interface InterfaceSubmenus {
    title:string;
    submenus:Array<InterfaceSudMenuItem>
}


export interface InterfaceSudMenuItem {
    icon:string;
    name:string;
    link:string;
}


export interface InterfaceMenusItem {
    super:InterfaceSubmenus[];
    bay:InterfaceSubmenus[];
}


export const menuItems:InterfaceMenusItem={
    super:[
        {
            title:'Control',
            submenus:[
                {
                    name:'Subscriptions',
                    icon:'fe-calendar',
                    link:'subscriptions'
                },
                {
                    name:'Users',
                    icon:'fe-users',
                    link:'users'
                },
                {
                    name:'Bays',
                    icon:'fe-server',
                    link:'bay'
                }
            ]
        }
    ],
    bay:[
        {
            title:'Sales',
            submenus:[
                {
                    name:'Sales',
                    icon:'fe-file-text',
                    link:'sales'
                },
                {
                    name:'Add Sale',
                    icon:'fe-shopping-bag',
                    link:'addsale'
                },
                {
                    name:'Invoices',
                    icon:'fe-file',
                    link:'invoices'
                },
                {
                    name:'Services',
                    icon:'fe-sliders',
                    link:'services'
                },
                {
                    name:'Washers',
                    icon:'fe-wind',
                    link:'washers'
                },
                {
                    name:'Customers',
                    icon:'fe-user-check',
                    link:'customers'
                },
                {
                    name:'Expenses',
                    icon:'fe-file',
                    link:'expenses'

                }
            ]
        },
        {
            title:'Control',
            submenus:[
                {
                    name:'Subscriptions',
                    icon:'fe-calendar',
                    link:'subscriptions'
                },
                {
                    name:'Users',
                    icon:'fe-users',
                    link:'users'
                }
            ]
        }
    ]
}