import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import routes from './home.routing';
import { ComponentsModule } from '../components/components.module';
import { SalesComponent } from './sales/sales.component';
import { NewSaleComponent } from './sales/newsale/newsale.component';
import { SalesFormComponent } from './sales/salesform/salesform.component';
import { UpdateSaleComponent } from './sales/updatesale/updatesale.component';
import { ServicesComponent } from './services/services.component';
import { WashersComponent } from './washers/washers.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AlertComponent } from '../components/alerts/alerts.component';
import { CustomersComponent } from './customers/customers.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { UsersComponent } from './users/users.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { BayComponent } from './bay/bay.component';
import { ProfileComponent } from '../profile/profile.component';
import { UserController } from '../controllers/user.controller';
import { InvoiceComponent } from './invoice/invoice.component';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { InvoiceListComponent } from './invoice/invoice-list.component';
import { InvoiceFormComponent } from './invoice/invoice-form.component';
import { EditSaleComponent } from './sales/edit-sale/edit-sale.component';
import { ExpensesComponent } from './expenses/expenses.component';




@NgModule({
    declarations: [
        HomeComponent,
        SalesComponent,
        NewSaleComponent,
        SalesFormComponent,
        UpdateSaleComponent,
        ServicesComponent,
        WashersComponent,
        SubscriptionsComponent,
        AlertComponent,
        CustomersComponent,
        DashboardComponent,
        UsersComponent,
        SubscribeComponent,
        BayComponent,
        ProfileComponent,
        InvoiceComponent,
        InvoiceListComponent,
        InvoiceFormComponent,
        EditSaleComponent,
        ExpensesComponent
    ],
    imports: [ 
        CommonModule,
        ComponentsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers:[
        UserController
    ],
})
export class HomeModule {}