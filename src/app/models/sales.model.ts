import { CustomerInterfaceModel } from "./customer.model";
import { OperationsInterfaceModel } from "./operations.model";
import { ServicesInterfaceModel } from "./services.model";
import { UsersInterfaceModel } from "./users.model";
import { WasherInterfaceModel } from "./washers.model";


export interface SaleSummaryItems {
    _id:any[];
    commissions:string;
    count:string;
    items:Array<SalesInterfaceModel>;
    profit:string;
    sales:string;
}


export interface SaleSummary {
    all:SaleSummaryItems[];
    summary:SaleSummaryItems[];
}

export interface SalesPayloadInterfaceModel {
    user:string;
    service:string;
    sendSms?:boolean;
    saleDate:string;
    commission:string;
    washers:string[];
    operations:string[];
    paymentType:string;
    total:string;
    customerName:string;
    customerPhone:string;
    serviceId:string;
}


export interface SalesInterfaceModel {
    _id:string;
    serviceId:string;
    saleDate:string;
    profit:string;
    edited?:boolean;
    editor?:UsersInterfaceModel;
    service:ServicesInterfaceModel;
    washers:WasherInterfaceModel[];
    operations:OperationsInterfaceModel[];
    customer:CustomerInterfaceModel;
    commission:string;
    washerCommission:string;
    paymentType:string;
    total:string;
    status:string;
    trackingNumber:string;
    createdAt:string;
    updatedAt:string;
}


export class SalesSummayModel implements SaleSummary {
    all:any=[];
    summary:any[];
    constructor(summary:SaleSummary){
        this.all=summary.all;
        this.summary=summary.summary;
    };

    getPaymentType(type:string){
        try {
            const sale=this.summary.find((p:any)=>{
                if(p._id===type){
                    return p;
                }
            });
            return sale?sale:{};
        } catch (err) {
            return {};
        }
    }

    getSummary(type:string){
        try {
            const sale=this.all.find((p:any)=>{
                const _f=Object.keys(p).find(key=>key===type);
                if(_f){
                    return p;
                }
            });
            return sale?sale[type]:'0';
        } catch (err) {
            return '0';
        }
    }

}


export class SalesModel implements SalesInterfaceModel {

    _id='';
    saleDate='';
    serviceId='';
    profit='';
    service;
    washers;
    operations;
    customer;
    commission='';
    washerCommission='';
    paymentType='';
    total='';
    status='';
    trackingNumber='';
    createdAt='';
    updatedAt='';
    edited=false;
    editor:UsersInterfaceModel=null;

    constructor(sale:SalesInterfaceModel){
        this._id=sale._id;
        this.saleDate=sale.saleDate;
        this.edited=sale.edited;
        this.editor=sale.editor;
        this.profit=sale.profit;
        this.serviceId=sale.serviceId;
        this.service=sale.service;
        this.washers=sale.washers;
        this.operations=sale.operations;
        this.customer=sale.customer;
        this.commission=sale.commission;
        this.washerCommission=sale.washerCommission;
        this.paymentType=sale.paymentType;
        this.total=sale.total;
        this.status=sale.status;
        this.trackingNumber=sale.trackingNumber;
        this.createdAt=sale.createdAt;
        this.updatedAt=sale.updatedAt;
    }

    getid(){
        return this._id.substring(0,10).toUpperCase();
    }

    getstatus(){
        switch (this.status) {
            case 'paid':
                return 'badge-success';
            case 'failed':
                return 'badge-danger';
            default:
                return 'badge-warning';
        }
    }

    getwashers(){
        return this.washers.map(w=>`${w.fname} ${w.lname}`);
    }


    getops(){
        return this.operations.map(w=>`${w.description}`);
    }


}