import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import { Store } from '@ngxs/store';
import { AppState } from '../state/app.state';
import { UsersInterfaceModel } from '../models/users.model';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    constructor (private store:Store) {}
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const user:UsersInterfaceModel|null=this.store.selectSnapshot(AppState.currentUser);
        req=req.clone({
            setHeaders: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.subscriptionToken || ''}`
            }
        })
        return next.handle(req);
    }


}