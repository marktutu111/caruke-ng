import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Action, Selector,State,StateContext, } from '@ngxs/store';
import { AlertModel } from '../models/alert.model';
import { BayInterface, BayModel } from '../models/bay.model';
import { CustomerInterfaceModel, CustomerModel } from '../models/customer.model';
import { Expense, ExpenseCategory, ExpenseModel, ExpenseQueryType, ExpenseResponse } from '../models/expenses.model';
import { Invoice, InvoiceModel } from '../models/invoice.model';
import { OperationsInterfaceModel, OperationsModel } from '../models/operations.model';
import ResponseInterfaceModel from '../models/response.model';
import { SalesInterfaceModel, SalesModel, SalesSummayModel, SaleSummary } from '../models/sales.model';
import { ServicesInterfaceModel, ServicesModel } from '../models/services.model';
import { Settings, SettingsModel } from '../models/settings.model';
import { SubscriptionInterface, SubscriptionModel } from '../models/subscription.model';
import { SummaryModel } from '../models/summary.model';
import { UserModel, UsersInterfaceModel } from '../models/users.model';
import { WasherInterfaceModel, WasherModel } from '../models/washers.model';
import { AdminService } from '../services/admin.service';
import { CustomerService } from '../services/customer.service';
import { ExpenseService } from '../services/expense.service';
import { InvoiceService } from '../services/invoice.service';
import { SalesService } from '../services/sales.service';
import { ServicesService } from '../services/services.service';
import { SettingsService } from '../services/settings.service';
import { UserService } from '../services/users.service';
import { WasherService } from '../services/washers.service';
import { FilterExpenses, FilterSales, GetBays, GetCustomers, GetExpenseCategory, GetExpenses, GetInvoices, GetOperations, GetSales, GetServices, GetSettings, GetSubs, GetSummary, GetUser, GetUsers, GetWashers, Logout, PushWashers, SearchCustomers, SelectInvoice, SelectSale, SetCurrentUser, SetState, ShowAlert } from './app.actions';


declare const Swal:any;


interface StateModel {
    loading:boolean;
    washers:WasherModel[];
    services:ServicesModel[];
    operations:OperationsModel[];
    customers:CustomerModel[];
    summary:SummaryModel | null;
    sales:SalesModel[];
    users:UserModel[] | null;
    user:UserModel | null;
    saleSummary:SaleSummary|any;
    bays:BayModel[] | null;
    subscriptions:SubscriptionModel[] | null;
    alert:AlertModel;
    count:string;
    settings:Settings|null;
    customerCount:String;
    selectedSale?:SalesModel[];
    selectedInvoice:InvoiceModel|null;
    invoices:InvoiceModel[];
    currentUser:UsersInterfaceModel|null;
    expenses:ExpenseResponse;
    expenseCategory:ExpenseCategory[];
}

@State<StateModel>({
    name:'appstate',
    defaults: {
        expenses:null,
        expenseCategory:[],
        customerCount:'',
        currentUser:null,
        selectedInvoice:null,
        invoices:[],
        selectedSale:[],
        saleSummary:{},
        loading:false,
        count:'0',
        settings:null,
        washers:[],
        sales:[],
        users:[],
        user:null,
        summary:null,
        customers:[],
        subscriptions:[],
        services:[],
        bays:[],
        operations:[],
        alert:{}
    }
})
@Injectable()
export class AppState {

    @Selector()
    static expenses(state:StateModel){
        return state.expenses;
    }


    @Selector()
    static expenseCategory(state:StateModel){
        return state.expenseCategory;
    }


    @Selector()
    static loading(state:StateModel){
        return state.loading;
    }

    @Selector()
    static count(state:StateModel){
        return state.count;
    }

    @Selector()
    static invoices(state:StateModel){
        return state.invoices;
    }

    @Selector()
    static selectedSale(state:StateModel){
        return state.selectedSale;
    }

    @Selector()
    static customerCount(state:StateModel){
        return state.customerCount;
    }

    @Selector()
    static saleSummary(state:StateModel){
        return state.saleSummary;
    }

    @Selector()
    static settings(state:StateModel){
        return state.settings;
    }

    @Selector()
    static currentUser(state:StateModel){
        return state.currentUser;
    }

    @Selector()
    static selectInvoice(state:StateModel){
        return state.selectedInvoice;
    }


    @Selector()
    static bays(state:StateModel){
        return state.bays;
    }

    @Selector()
    static subscriptions(state:StateModel){
        return state.subscriptions;
    }

    @Selector()
    static customers(state:StateModel){
        return state.customers;
    }

    @Selector()
    static users(state:StateModel){
        return state.users;
    }

    @Selector()
    static user(state:StateModel){
        return state.user;
    }

    @Selector()
    static summary(state:StateModel){
        return state.summary;
    }

    @Selector()
    static sales(state:StateModel){
        return state.sales;
    }

    @Selector()
    static washers(state:StateModel){
        return state.washers;
    }

    @Selector()
    static services(state:StateModel){
        return state.services;
    }

    @Selector()
    static operations(state:StateModel){
        return state.operations;
    }

    @Selector()
    static alert(state:StateModel){
        return state.alert;
    }

    constructor(
        private _services:ServicesService,
        private _wservice:WasherService,
        private _saless:SalesService,
        private cservice:CustomerService,
        private userService:UserService,
        private adminServiice:AdminService,
        private settingsService:SettingsService,
        private invoiceService:InvoiceService,
        private _EService:ExpenseService
    ){};


    @Action(SetState)
    setState({patchState}:StateContext<StateModel>,{payload}:SetState){
        patchState(payload);
    }


    @Action(PushWashers)
    pushWasher({patchState}:StateContext<StateModel>,{payload}:PushWashers){
        patchState(
            {
                washers:payload
            }
        )
    }


    @Action(GetExpenseCategory)
    async getExpenseCategory({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            )
            const response=await this._EService.getExpenseCategory();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            patchState(
                {
                    loading:false,
                    expenseCategory:response.data
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetExpenses)
    async getExpenses({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            )
            let response=await this._EService.getExpenses();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            response.data.transactions=response.data.transactions.map((tr:Expense)=>new ExpenseModel(tr));
            patchState(
                {
                    loading:false,
                    expenses:response.data
                }
            )
        } catch (err) {
            console.log(err);
            
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(SetCurrentUser)
    setUser({patchState}:StateContext<StateModel>,{data,rem}:SetCurrentUser){
        patchState(
            {
                currentUser:data
            }
        );
        if(rem){
            localStorage.setItem(
                'caruke-auth',
                JSON.stringify(data)
            )
        }
    }


    @Action(Logout)
    logout({patchState,dispatch}:StateContext<StateModel>){
        patchState(
            {
                user:null,
                bays:[],
                currentUser:null,
                customers:[],
                operations:[],
                saleSummary:{},
                sales:[],
                services:[],
                settings:null,
                subscriptions:[],
                summary:null,
                users:[],
                washers:[],
                loading:false,
                count:'0'
            }
        );
        dispatch(
            new Navigate(
                ['../login']
            )
        );
        try {
            localStorage.removeItem(
                'caruke-auth'
            )
        } catch (err) {
            console.log('no user stored');
        }
    }


    @Action(GetInvoices)
    async getInvoices({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            )
            const response=await this.invoiceService.getInvoices();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const invoices=response.data.items.map((i:Invoice)=>new InvoiceModel(i));
            patchState(
                {
                    loading:false,
                    invoices:invoices
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetSettings)
    async getSettings({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            )
            const response=await this.settingsService.getsetns();
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            patchState(
                {
                    loading:false,
                    settings:new SettingsModel(response.data)
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetBays)
    async getBay({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            )
            const response=await this.adminServiice.getbays();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const bays=response.data.map((bay:BayInterface)=>new BayModel(bay));
            patchState(
                {
                    loading:false,
                    bays:bays
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetSubs)
    async getSubs({patchState,getState}:StateContext<StateModel>){
        try {
            const {currentUser}=getState();
            patchState(
                {
                    loading:true
                }
            );
            let response:ResponseInterfaceModel;
            switch (currentUser?.account) {
                case 'bay':
                    response=await this.adminServiice.getBaysubs(currentUser.bay._id||'');
                    break;
                default:
                    response=await this.adminServiice.getsubs();
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const subs=response.data.map((sub:SubscriptionInterface)=>new SubscriptionModel(sub));
            patchState(
                {
                    loading:false,
                    subscriptions:subs
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }

    @Action(GetSummary)
    async getSummary({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            )
            const response=await this._saless.summary();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const _summ=new SummaryModel(response.data);
            patchState(
                {
                    loading:false,
                    summary:_summ
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetUsers)
    async getUsers({patchState,getState}:StateContext<StateModel>){
        try {
            const {currentUser}=getState();
            patchState(
                {
                    loading:true
                }
            );
            const response:ResponseInterfaceModel=await this.userService.getall();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const _users=response.data.items?.map((user:UsersInterfaceModel)=>new UserModel(user));
            patchState(
                {
                    loading:false,
                    users:_users,
                    count:response.data.count
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetUser)
    async getUser({patchState}:StateContext<StateModel>,{id}:GetUser){
        try {
            patchState(
                {
                    loading:true
                }
            )
            let response:ResponseInterfaceModel=await this.userService.getone(id);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            const _user=new UserModel(response.data);
            patchState(
                {
                    loading:false,
                    user:_user
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(SearchCustomers)
    async searchCustomers({patchState}:StateContext<StateModel>,{search}:SearchCustomers){
        try {
            patchState(
                {
                    loading:true
                }
            )
            const response=await this.cservice.searchCustomer(search);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            const customers=response.data?.items.map((c:CustomerInterfaceModel)=>new CustomerModel(c));
            patchState(
                {
                    loading:false,
                    customers:customers,
                    customerCount:response.data?.count
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }

    @Action(FilterExpenses)
    async filterExpenses({patchState}:StateContext<StateModel>,{data}:FilterExpenses){
        try {
            patchState(
                {
                    loading:true
                }
            )
            let string=Object.keys(data).map((_k:string)=>data[_k] && `${_k}=${data[_k]}`).join("&");
            console.log(string);
            const response=await this._EService.filterExpenses(string);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            response.data.transactions=response.data.transactions.map((tr:Expense)=>new ExpenseModel(tr));
            patchState(
                {
                    loading:false,
                    expenses:response.data
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetCustomers)
    async getCustomers({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            );
            const response=await this.cservice.getcustomers();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const customers=response.data?.items.map((c:CustomerInterfaceModel)=>new CustomerModel(c));
            patchState(
                {
                    loading:false,
                    customers:customers,
                    customerCount:response.data?.count
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }

    
    @Action(SelectSale)
    selectSale({patchState}:StateContext<StateModel>,{sale}:SelectSale){
        patchState(
            {
                selectedSale:sale
            }
        )
    }


    @Action(SelectInvoice)
    selectInvoice({patchState}:StateContext<StateModel>,{invoice}:SelectInvoice){
        patchState(
            {
                selectedInvoice:invoice
            }
        )
    }


    @Action(FilterSales)
    async filterSales({patchState}:StateContext<StateModel>,{data}:FilterSales){
        try {
            patchState(
                {
                    loading:true
                }
            );
            patchState(
                {
                    saleSummary:{},
                    sales:[]
                }
            )
            const response=await this._saless.filter(data);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const summay:SalesSummayModel=new SalesSummayModel(response.data);
            const _sales=summay.all[0]?.items.map((d:SalesInterfaceModel)=>new SalesModel(d));
            patchState(
                {
                    loading:false,
                    saleSummary:summay,
                    sales:_sales
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetSales)
    async getSales({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            );
            patchState(
                {
                    saleSummary:{},
                    sales:[]
                }
            )
            const response=await this._saless.getsales();
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const summay:SummaryModel=new SummaryModel(response.data);
            const _sales=summay.all[0]?.items.map((d:SalesInterfaceModel)=>new SalesModel(d));
            patchState(
                {
                    loading:false,
                    saleSummary:summay,
                    sales:_sales
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetOperations)
    async getOperations({patchState}:StateContext<StateModel>,{id}:GetOperations){
        try {
            patchState(
                {
                    loading:true
                }
            );
            let response:ResponseInterfaceModel;
            switch (typeof id=='string') {
                case true:
                    response=await this._services.getOperationsByService(id!);
                    break;
                default:
                    response=await this._services.getOperations();
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message 
                )
            };
            const _d=response.data.map((op:OperationsInterfaceModel)=>new OperationsModel(op));
            patchState(
                {
                    loading:false,
                    operations:_d
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetServices)
    async getServices({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            );
            const response=await this._services.getsvs();
            if(!response.success){
                throw Error(
                    response.message 
                )
            };
            const _d=response.data.map((op:ServicesInterfaceModel)=>new ServicesModel(op));
            patchState(
                {
                    loading:false,
                    services:_d
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }

    @Action(GetWashers)
    async getWashers({patchState}:StateContext<StateModel>){
        try {
            patchState(
                {
                    loading:true
                }
            );
            const response=await this._wservice.get();
            if(!response.success){
                throw Error(
                    response.message 
                )
            };
            const _d=response.data.map((op:WasherInterfaceModel)=>new WasherModel(op));
            patchState(
                {
                    loading:false,
                    washers:_d
                }
            )
        } catch (err) {
            
        }
    }



    @Action(ShowAlert)
    showAlert({}:StateContext<StateModel>,{payload}:ShowAlert){
        Swal.fire(
            {
                position:'top-end',
                icon:payload.type,
                title:payload.message,
                showConfirmButton:false,
                timer: 1500
            }
        )
    }





}
