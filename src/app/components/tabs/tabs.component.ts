import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

    @Input() tabs:string[]=[];
    @Input() selected:string='';
    @Output() change:EventEmitter<string>=new EventEmitter();

    constructor() {}

    ngOnInit(): void {}

    onTab(s:string){
        this.change.emit(s);
    }


}

