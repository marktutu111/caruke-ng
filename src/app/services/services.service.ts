import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import ResponseInterfaceModel from '../models/response.model';
import api from '../constants/api';
import { ServicesPayloadIntefaceModel } from '../models/services.model';
import { OperationsPayloadIntefaceModel } from '../models/operations.model';

@Injectable({
    providedIn: 'root'
})
export class ServicesService {

    constructor(private http:HttpClient){};

    adds(data:ServicesPayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/services/add`,data)
            .toPromise()
            .then(res=>res)
            .catch(err=>err)
    }

    addOperation(data:OperationsPayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/operations/add`,data)
            .toPromise()
            .then(res=>res)
            .catch(err=>err)
    }

    getsvs():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/services/get`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    getOperations():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/operations/get`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    getOperationsByService(id:string):Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/operations/service/get/${id}`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }

    deleteOp(id:string):Promise<ResponseInterfaceModel>{
        return this.http.delete(`${api.user}/operations/delete/${id}`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err)
    }


}