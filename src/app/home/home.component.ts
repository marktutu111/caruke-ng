import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetSettings } from '../state/app.actions';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(private store:Store) {};

    ngOnInit(): void {
        this.store.dispatch(
            new GetSettings()
        )
    };


}
