import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BayInterface, BayModel } from 'src/app/models/bay.model';
import { UsersInterfaceModel } from 'src/app/models/users.model';
import { AdminService } from 'src/app/services/admin.service';
import { GetBays, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-subscribe',
    templateUrl: './subscribe.component.html',
    styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

    @Select(AppState.bays) bays$:Observable<BayModel[]>;
    @Select(AppState.currentUser) currentUser$:Observable<UsersInterfaceModel>;

    formGroup:FormGroup=this.fb.group(
        {
            user:new FormGroup(
                {
                    duration:new FormControl('',Validators.required),
                    durationType:new FormControl('',Validators.required),
                    accountNumber:new FormControl('',Validators.required),
                    accountIssuer:new FormControl('',Validators.required),
                    otp:new FormControl('',Validators.required)
                }
            ),
            admin:new FormGroup(
                {
                    duration:new FormControl('',Validators.required),
                    durationType:new FormControl('',Validators.required),
                    bay:new FormControl('',Validators.required)
                }
            )
        }
    )

    loading:boolean=false;

    constructor(private store:Store,private service:AdminService,private fb:FormBuilder) {};


    ngOnInit():void {
        this.store.dispatch(
            new GetBays()
        )
    };


    async sendOtp(){
        try {
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            const number=this.formGroup.get('user')?.get('accountNumber');
            if(!number?.valid){
                throw Error(
                    'Please enter a valid mobile money number'
                )
            }
            this.loading=true;
            const response=await this.service.sendOtp(
                {
                    account:number.value
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:'OTP sent to your phone',
                        type:'success'
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


    async save(){
        try {
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            const subscription:any=this.formGroup.get('user');
            if(!subscription.valid){
                throw Error(
                    'Please enter all the required data'
                )
            };
            this.loading=true;
            const response=await this.service.subscribe(subscription.value);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'success',
                        message:response.message,
                        show:true
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'danger',
                        message:err.message,
                        show:true
                    }
                )
            )
        }
    }


    async issue(){
        try {
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            );
            const subscription:any=this.formGroup.get('admin');
            if(!subscription.valid){
                throw Error(
                    'Please enter all the required data'
                )
            };
            this.loading=true;
            const response=await this.service.issue(subscription.value);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'success',
                        message:response.message,
                        show:true
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'danger',
                        message:err.message,
                        show:true
                    }
                )
            )
        }
    }



}
