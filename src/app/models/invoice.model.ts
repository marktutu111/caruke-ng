import { BayInterface } from "./bay.model";
import { CustomerInterfaceModel } from "./customer.model";
import { SalesInterfaceModel } from "./sales.model";
import { UsersInterfaceModel } from "./users.model";


export interface InvoicePayload {
    user:string;
    bay:string;
    customer:string;
    total:string;
    vat:string;
    subtotal:string;
    status:string;
    sales:Array<string>
}


export interface Invoice {
    _id:string;
    bay:BayInterface;
    user:UsersInterfaceModel;
    customerName:string;
    customerPhone:string;
    customerAddress:string;
    total:string;
    vat:string;
    subtotal:string;
    status:string;
    sales:Array<SalesInterfaceModel>;
    datePaid:string,
    createdAt:string;
    updatedAt:string;
}


export class InvoiceModel implements Invoice {
    
    _id='';
    bay:BayInterface;
    user:UsersInterfaceModel;
    customerName='';
    customerPhone='';
    customerAddress='';
    total='';
    vat='';
    subtotal='';
    status='';
    sales:SalesInterfaceModel[]=[];
    createdAt='';
    datePaid='';
    updatedAt='';


    constructor(invoice:Invoice){
        this._id=invoice._id;
        this.user=invoice.user;
        this.customerName=invoice.customerName;
        this.customerPhone=invoice.customerPhone;
        this.customerAddress=invoice.customerAddress;
        this.bay=invoice.bay;
        this.total=invoice.total;
        this.vat=invoice.vat;
        this.subtotal=invoice.subtotal;
        this.status=invoice.status;
        this.sales=invoice.sales;
        this.createdAt=invoice.createdAt;
        this.datePaid=invoice.datePaid;
    }



}