
export interface CustomerPayloadInterfaceModel {
    name:string;
    phone:string;
    birthMonth?:string;
    birthDay?:string;
}


export interface CustomerInterfaceModel {
    _id:string;
    name:string;
    phone:string;
    blocked:boolean;
    createdAt:string;
    updatedAt:string;
    birthMonth?:string;
    birthDay?:string;
}


export class CustomerModel implements CustomerInterfaceModel {

    _id='';
    name='';
    phone='';
    blocked=false;
    createdAt='';
    updatedAt='';
    birthDay:string;
    birthMonth:string;

    constructor(customer:CustomerInterfaceModel){
        this._id=customer._id;
        this.birthDay=customer.birthDay;
        this.birthMonth=customer.birthMonth;
        this.name=customer.name;
        this.phone=customer.phone;
        this.blocked=customer.blocked;
        this.createdAt=customer.createdAt;
        this.updatedAt=customer.updatedAt;
    }


}