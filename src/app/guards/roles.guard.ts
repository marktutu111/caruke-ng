import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppState } from '../state/app.state';

@Injectable({
    providedIn: 'root'
})
export class RolesGuard implements CanActivate {

    constructor(private store:Store){};

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const user:any=this.store.selectSnapshot(AppState.currentUser);
        const path=route.routeConfig?.path;
        switch (path) {
            case 'dashboard':
                if(user.account==='bay'){
                    return true;
                }
                break;
            case 'subscribe':
                return true;
            case 'profile':
                return true;
            case 'invoice':
                if(user.account==='bay'){
                    return true;
                }
                break;
            case 'editsale':
                if(user.account==='bay'){
                    return true;
                }
                break;
            case 'newinvoice':
                if(user.account==='bay'){
                    return true;
                }
                break;
            default:
                if(user?.role.indexOf(path)>-1){
                    return true;
                }
                break;
        }
        return false;

    }


}
