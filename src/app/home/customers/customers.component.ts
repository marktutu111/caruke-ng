import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CustomerInterfaceModel, CustomerModel, CustomerPayloadInterfaceModel } from 'src/app/models/customer.model';
import ResponseInterfaceModel from 'src/app/models/response.model';
import { CustomerService } from 'src/app/services/customer.service';
import { GetCustomers, SearchCustomers, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import *as moment from "moment";


@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

    @Select(AppState.loading) loading$:Observable<boolean>;
    @Select(AppState.customerCount) customerCount$:Observable<String>;
    @Select(AppState.customers) customers$:Observable<CustomerModel[]>;

    months:string[]=Array.apply(0, Array(12)).map(function(_,i){return moment().month(i).format('MMM')});
    days:number[]=Array.apply(0,Array(31)).map((_,i)=>i+1);

    p:number=1;

    formGroup:FormGroup=this.fb.group(
        {
            customer:new FormGroup(
                {
                    _id:new FormControl(''),
                    name:new FormControl('',Validators.required),
                    phone:new FormControl('',Validators.required),
                    birthMonth:new FormControl(null),
                    birthDay:new FormControl(null)
                }
            ),
            search:new FormControl('',Validators.required)
        }
    )


    loading:boolean=false;
    onedit:boolean=false;

    constructor(private store:Store,private service:CustomerService,private fb:FormBuilder) {};

    ngOnInit(): void {
        this.refresh();
    };

    
    refresh(){
        this.store.dispatch(
            new GetCustomers()
        )
    }


    filter(){
        if(!this.formGroup.get('search')?.valid){
            return;
        };
        this.store.dispatch(
            new SearchCustomers(
                this.formGroup.get('search')?.value
            )
        )
    }


    async save(){
        try {
            const form=this.formGroup.get('customer');
            if(!form?.valid){
                throw Error(
                    'Please fill the form and  provide all the required data'
                )
            };
            this.loading=true;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            let response:ResponseInterfaceModel;
            const data:CustomerInterfaceModel=form.value;
            const payload:CustomerPayloadInterfaceModel={
                name:data.name,
                phone:data.phone,
                birthDay:data.birthDay,
                birthMonth:data.birthMonth
            }
            switch (this.onedit) {
                case true:
                    response=await this.service.update(
                        { id:data._id,data:payload}
                    );
                    break;
                default:
                    response=await this.service.addcustomer(payload);
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.onedit=false;
            this.formGroup.reset();
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            show:true,
                            message:response.message,
                            type:'success'
                        }
                    ),
                    new GetCustomers()
                ]
            );
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }



}
