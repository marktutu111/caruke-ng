
export interface AlertModel {
    type?:'success'|'danger'|'warning'|'error';
    message?:string;
    show?:boolean;
}