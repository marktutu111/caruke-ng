import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InvoiceModel } from '../models/invoice.model';
import { AppState } from '../state/app.state';

@Injectable({
    providedIn: 'root'
})
export class InvoiceGuard implements CanActivate {

    constructor(private store:Store){}
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const selected:InvoiceModel=this.store.selectSnapshot(AppState.selectInvoice);
        if(selected && selected._id){
            return true;
        }
        return false;
    }


}
