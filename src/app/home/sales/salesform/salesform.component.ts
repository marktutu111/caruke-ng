import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CustomerInterfaceModel, CustomerModel } from 'src/app/models/customer.model';
import { OperationsModel } from 'src/app/models/operations.model';
import ResponseInterfaceModel from 'src/app/models/response.model';
import { SalesModel, SalesPayloadInterfaceModel } from 'src/app/models/sales.model';
import { ServicesModel } from 'src/app/models/services.model';
import { WasherModel } from 'src/app/models/washers.model';
import { SalesService } from 'src/app/services/sales.service';
import { GetCustomers, GetOperations, GetServices, GetWashers, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';


@Component({
    selector: 'app-salesform',
    templateUrl: './salesform.component.html',
    styleUrls: ['./salesform.component.scss']
})
export class SalesFormComponent implements OnInit {

    @Select(AppState.services) services$:Observable<ServicesModel[]>;
    @Select(AppState.operations) operations$:Observable<OperationsModel[]>;
    @Select(AppState.washers) washers$:Observable<WasherModel[]>;
    @Select(AppState.customers) customers$:Observable<CustomerModel[]>;

    @Input() type:'new'|'update'='new';

    private userId:string=this.store.selectSnapshot(AppState.currentUser)._id;
    private saleId:string=null;
    loading:boolean=false;

    formGroup:FormGroup=new FormGroup(
        {
            user:new FormControl(this.userId,Validators.required),
            service:new FormControl('',Validators.required),
            sendSms:new FormControl(false),
            commission:new FormControl(0,Validators.required),
            washers:new FormControl([],Validators.required),
            operations:new FormControl([],Validators.required),
            paymentType:new FormControl('',Validators.required),
            total:new FormControl(0,Validators.required),
            customerName:new FormControl(''),
            customerPhone:new FormControl(''),
            saleDate:new FormControl(new Date((new Date()).valueOf()).toLocaleDateString('en-CA'),Validators.required),
            serviceId:new FormControl(''),
            status:new FormControl(''),
            trackingNumber:new FormControl(''),
            customer:new FormControl('')
        }
    )


    constructor(private store:Store,private service:SalesService) {};


    ngOnInit(): void {
        this.store.dispatch(
            [
                new GetWashers(),
                new GetServices(),
                new GetCustomers()
            ]
        );

        if(this.type==='update'){
            try {
                const sale:SalesModel=this.store.selectSnapshot(AppState.selectedSale)[0];
                this.store.dispatch(new GetOperations(sale?.service._id));  
                this.saleId=sale._id;  
                this.formGroup.patchValue(
                    {
                        service:sale.service?._id,
                        commission:sale.commission,
                        washers:sale.washers.map(w=>w._id),
                        operations:sale.operations.map(o=>o._id),
                        paymentType:sale.paymentType,
                        total:sale.total,
                        customerName:sale.customer?.name || '',
                        customerPhone:sale.customer?.phone || '',
                        saleDate:new Date((new Date(sale.createdAt)).valueOf()).toLocaleDateString('en-CA'),
                        serviceId:sale.serviceId,
                        status:sale.status,
                        trackingNumber:sale.trackingNumber,
                        customer:sale.customer?._id
                    }
                )
            } catch (err) {}
        }

    };


    onWasherCheck(washer:WasherModel,e:any){
        let washers:string[]=this.formGroup.get('washers')?.value;
        switch (e.checked) {
            case true:
                washers=[...washers,washer._id];
                break;
            default:
                washers=washers.filter(w=>w!==washer._id);
                break;
        }
        this.formGroup.patchValue(
            {
                washers:washers
            }
        )
    }

    onOperationCheck(op:OperationsModel,e:any){
        let ops:string[]=this.formGroup.get('operations')?.value;
        let total=this.formGroup.get('total')?.value;
        switch (e.checked) {
            case true:
                ops=[...ops,op._id];
                total=total+parseFloat(op.amount);
                break;
            default:
                ops=ops.filter(w=>w!==op._id);
                total=total-parseFloat(op.amount)
                break;
        }
        this.formGroup.patchValue(
            {
                operations:ops,
                total:total
            }
        )
    }


    onService(e:any){
        try {
            const s=e.options[e.selectedIndex].getAttribute('service');
            const _s:ServicesModel=JSON.parse(s);
            this.formGroup.patchValue(
                {
                    commission:_s.commission
                }
            );
            this.store.dispatch(
                new GetOperations(
                    _s._id
                )
            )
        } catch (err) {
            this.formGroup.patchValue(
                {
                    commission:0
                }
            )
        };
    }


    async save(){
        try {
            if(!this.formGroup.valid){
                throw Error(
                    'Please provide all data'
                )
            };
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            );
            const _form=this.formGroup.value;
            if(_form.paymentType==='credit' && (_form.customerName==='' || _form.customerPhone==='')){
                throw Error(
                    'Oops! please enter customer name and phone number for a credit sales.'
                )
            }

            this.loading=true;
            let response:ResponseInterfaceModel=null;
            let _payload:SalesPayloadInterfaceModel={
                commission:_form.commission,
                customerName:_form.customerName,
                customerPhone:_form.customerPhone,
                operations:_form.operations,
                paymentType:_form.paymentType,
                saleDate:_form.saleDate,
                service:_form.service,
                serviceId:_form.serviceId,
                total:_form.total,
                washers:_form.washers,
                sendSms:_form.sendSms,
                user:_form.user
            }

            switch (this.type) {
                case 'update':
                    response=await this.service.updatesale(
                        {
                            id:this.saleId,
                            data:{..._form,edited:true,editor:this.userId}
                        }
                    );
                    break;
                default:
                    response=await this.service.addsale(_payload);
                    break;
            }

            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.saleId='';
            this.formGroup.patchValue(
                {
                    user:this.userId,
                    service:'',
                    sendSms:false,
                    commission:0,
                    washers:[],
                    operations:[],
                    paymentType:'',
                    total:0,
                    customerName:'',
                    customerPhone:'',
                    saleDate:new Date((new Date()).valueOf() - 1000*60*60*24).toLocaleDateString('en-us'),
                    serviceId:'',
                    customer:'',
                    status:'',
                    trackingNumber:''
                }
            )
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            show:true,
                            message:'Sale recorded and processed successfully',
                            type:'success'
                        }
                    ),
                    new GetCustomers()
                ]
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message
                    }
                )
            )
        }
    }


    onCustomer(e:any){
        try {
            const customer:CustomerInterfaceModel=JSON.parse(e.options[e.selectedIndex].getAttribute('customer'));
            this.formGroup.patchValue(
                {
                    customerName:customer.name,
                    customerPhone:customer.phone
                }
            )
        } catch (err) {
            this.formGroup.patchValue(
                {
                    customerName:'',
                    customerPhone:''
                }
            )
        }
    }


}
