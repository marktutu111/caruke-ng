import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ExpenseCategory, ExpenseResponse } from 'src/app/models/expenses.model';
import { ExpenseService } from 'src/app/services/expense.service';
import { FilterExpenses, GetExpenseCategory, GetExpenses, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import *as moment from "moment";

@Component({
    selector: 'app-expenses',
    templateUrl: './expenses.component.html',
    styleUrls: ['./expenses.component.scss']
})
export class ExpensesComponent implements OnInit {

    @Select(AppState.expenses) expenses$:Observable<ExpenseResponse>;
    @Select(AppState.expenseCategory) categories$:Observable<ExpenseCategory[]>;
    @Select(AppState.loading) loading$:Observable<boolean>;

    loading:Boolean=false;
    p:number=1;
    s:number=1;
    selected:any[]=[];

    formGroup=this.fb.group(
        {
            category:new FormGroup(
                {
                    name:new FormControl(null,Validators.required),
                    description:new FormControl(null,Validators.required)
                }
            ),
            expense:new FormGroup(
                {
                    category:new FormControl(null,Validators.required),
                    amount:new FormControl(null,Validators.required),
                    tax:new FormControl(0),
                    reference:new FormControl(null),
                    notes:new FormControl(null,Validators.required),
                    expenseDate:new FormControl(new Date((new Date()).valueOf()).toLocaleDateString('en-CA'),Validators.required)
                }
            ),
            filter:new FormGroup(
                {
                    start:new FormControl(null),
                    end:new FormControl(null),
                    account:new FormControl(null)
                }
            )
        }
    )


    constructor(private fb:FormBuilder,private store:Store,private _EService:ExpenseService) {}


    ngOnInit(): void {
        this.store.dispatch(
            [
                new GetExpenseCategory(),
                new GetExpenses()
            ]
        )
    };


    async saveExpense(){
        try {
            const data=this.formGroup.get('expense');
            if(!data.valid){
                return;
            }
            this.loading=true;
            const response=await this._EService.addExpense(data.value);
            this.loading=false;
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            data.reset();
            data.patchValue({expenseDate:new Date((new Date()).valueOf()).toLocaleDateString('en-CA'),tax:0})
            this.store.dispatch(
                [
                    new GetExpenses(),
                    new ShowAlert(
                        {
                            message:'Expense account added successfully',
                            type:'success'
                        }
                    )
                ]
            )
        } catch (err:any) {
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'danger',
                        message:err.message
                    }
                )
            )
        }
    }


    async saveCategory(){
        try {
            const data=this.formGroup.get('category');
            if(!data.valid){
                return;
            }
            this.loading=true;
            const response=await this._EService.addExpenseCategory(data.value);
            this.loading=false;
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            data.reset();
            this.store.dispatch(
                [
                    new GetExpenseCategory(),
                    new ShowAlert(
                        {
                            message:'Expense account added successfully',
                            type:'success'
                        }
                    )
                ]
            )
        } catch (err:any) {
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'danger',
                        message:err.message
                    }
                )
            )
        }
    }

    onDuration(event:any){
        try {
            const _duration:moment.Duration=event.value;
            if(typeof _duration!=='string')return;
            this.formGroup.get('filter').patchValue(
                {
                    start:moment(new Date()).startOf(_duration).toDate(),
                    end:moment(new Date()).endOf(_duration).toDate()
                }
            )
        } catch (err) {}
    }

    filter(){
        const data=this.formGroup.get('filter');
        console.log(data.value);
        this.store.dispatch(
            new FilterExpenses(
                data.value
            )
        )
    }

    async deleteExpense(){
        try {
            if(!this.selected[0])return;
            this.loading=true;
            let _cI=this.selected.shift();
            const response=await this._EService.deleteExpense(_cI);
            this.loading=false;
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            switch (this.selected.length>0) {
                case true:
                    this.deleteExpense();
                    break;
                default:
                    this.store.dispatch(
                        [
                            new GetExpenses(),
                            new ShowAlert(
                                {
                                    message:response.message,
                                    type:'success'
                                }
                            )
                        ]
                    );
                    break;
            }
        } catch (err:any) {
            this.store.dispatch(
                new ShowAlert(
                    {
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


    async deleteCategory(){
        try {
            if(!this.selected[0])return;
            this.loading=true;
            let _cI=this.selected.shift();
            const response=await this._EService.deleteExpenseCategory(_cI);
            this.loading=false;
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            switch (this.selected.length>0) {
                case true:
                    this.deleteCategory();
                    break;
                default:
                    this.store.dispatch(
                        [
                            new GetExpenseCategory(),
                            new ShowAlert(
                                {
                                    message:response.message,
                                    type:'success'
                                }
                            )
                        ]
                    );
                    break;
            }
        } catch (err:any) {
            this.store.dispatch(
                new ShowAlert(
                    {
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


    onCheck(sel:any,id:string){
        const checked=sel.checked;
        switch (checked) {
            case false:
                this.selected=this.selected.filter(sel=>sel!==id);
                break;
            default:
                this.selected.push(id);
                break;
        }
    }



}
