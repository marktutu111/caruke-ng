import { ServicesInterfaceModel } from "./services.model";

export interface OperationsPayloadIntefaceModel {
    service:string;
    amount:string;
    description:string;
}

export interface OperationsInterfaceModel {
    _id:string;
    service:ServicesInterfaceModel;
    amount:string;
    description:string;
    createdAt:string;
    updatedAt:string;
}

export class OperationsModel implements OperationsInterfaceModel {

    _id='';
    service;
    amount='';
    description='';
    createdAt='';
    updatedAt='';
    checked:boolean=false;

    constructor(operation:OperationsInterfaceModel){
        this._id=operation._id;
        this.service=operation.service;
        this.amount=operation.amount;
        this.description=operation.description;
        this.createdAt=operation.createdAt;
        this.updatedAt=operation.updatedAt;
    };

    onCheck(v:any){
        this.checked=v.checked
    }


}