import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { UsersInterfaceModel } from '../models/users.model';
import { AppState } from '../state/app.state';

@Injectable({
    providedIn: 'root'
})
export class HomeGuard implements CanActivate {

    constructor(private store:Store,private router:Router){};

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const user:UsersInterfaceModel|null=this.store.selectSnapshot(AppState.currentUser);
        if(user&&user._id){
            return true;
        }
        this.router.navigate(['login']);
        return false;
    }

}
