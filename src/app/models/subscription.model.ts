import { BayPayloadInterface } from "./bay.model";

export interface SubscriptionPayloadInterface {
    bay:string;
    duration:string | number;
    durationType:string;
    accountNumber:string;
    accountIssuer:string;
    otp:string;
}



export interface SubscriptionIssueInteface {
    duration:string;
    durationType:string;
    bay:string;
}



export interface SubscriptionInterface {
    _id:string;
    bay:BayPayloadInterface|null;
    daysRemaining:string;
    amount:number;
    startDate:string;
    endDate:string;
    code:string;
    cpd:string;
    nod:string;
    token:string;
    status:string;
    createdAt:string;
    updatedAt:string;
}


export class SubscriptionModel implements SubscriptionInterface {

    _id='';
    bay;
    amount=0;
    startDate='';
    endDate='';
    code='';
    cpd='';
    nod='';
    status='';
    token:string;
    createdAt='';
    updatedAt='';
    daysRemaining='';

    constructor(subscription:SubscriptionInterface){
        this._id=subscription._id;
        this.bay=subscription.bay;
        this.nod=subscription.nod;
        this.token=subscription.token;
        this.amount=subscription.amount;
        this.startDate=subscription.startDate;
        this.endDate=subscription.endDate;
        this.code=subscription.code;
        this.cpd=subscription.cpd;
        this.status=subscription.status;
        this.daysRemaining=subscription.daysRemaining;
        this.createdAt=subscription.createdAt;
        this.updatedAt=subscription.updatedAt;
    }

}