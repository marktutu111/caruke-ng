import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { UserController } from '../controllers/user.controller';
import { InterfaceMenusItem, menuItems } from '../models/menus.model';
import { UserModel } from '../models/users.model';
import { Logout } from '../state/app.actions';
import { AppState } from '../state/app.state';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    @Select(AppState.currentUser) currentUser$:Observable<UserModel>;

    tabs:string[]=[];
    index:string='profile';
    menus:InterfaceMenusItem=menuItems;


    constructor(private store:Store,public controller:UserController) {};


    ngOnInit(): void {
        const user=this.store.selectSnapshot(AppState.currentUser);
        const settings=this.store.selectSnapshot(AppState.settings);
        this.controller.formGroup.patchValue(
            {
                profile:user,
                settings:settings,
                password:{
                    id:user._id
                }
            }
        )

        switch (user?.account) {
            case 'bay':
                this.tabs=['profile','settings','password'];
                break;
            default:
                this.tabs=['profile','password'];
                break;
        }

    };

    logout(){
        this.store.dispatch(
            new Logout()
        )
    }

    onTab(s:string){this.index=s}


}
