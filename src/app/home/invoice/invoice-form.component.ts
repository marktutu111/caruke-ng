import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { InvoicePayload } from 'src/app/models/invoice.model';
import { SalesModel } from 'src/app/models/sales.model';
import { InvoiceService } from 'src/app/services/invoice.service';
import { ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-invoice-form',
    templateUrl: './invoice-form.component.html',
    styleUrls: ['./invoice-form.component.scss']
})
export class InvoiceFormComponent implements OnInit,OnDestroy {

    loading:boolean=false;
    formGroup:FormGroup=new FormGroup(
        {
            user:new FormControl('',Validators.required),
            bay:new FormControl('',Validators.required),
            customerName:new FormControl('',Validators.required),
            customerPhone:new FormControl('',Validators.required),
            customerAddress:new FormControl(''),
            total:new FormControl(0,Validators.required),
            vat:new FormControl(0,Validators.required),
            subtotal:new FormControl('',Validators.required),
            status:new FormControl('pending'),
            sales:new FormControl('',Validators.required)
        }
    )

    private subscription$:Subscription;


    constructor(private store:Store,private invoice:InvoiceService) {};

    ngOnInit(): void {
        this.setDefault();
        this.subscription$=this.formGroup.valueChanges.subscribe(({vat,subtotal})=>{
            try {
                if(typeof vat==='number'){
                    const amount=(vat/100)*subtotal;
                    this.formGroup.patchValue(
                        {
                            total:subtotal-amount
                        },
                        {
                            emitEvent:false,
                            onlySelf:true
                        }
                    )
                }
            } catch (err) {}
        })
    };


    setDefault(){
        let subtotal:number=0;
        let sales:string[]=[];
        let customerName:string='';
        let customerPhone:string='';
        const {selectedSale,currentUser}=this.store.selectSnapshot(AppState);
        selectedSale.forEach((sale:SalesModel)=>{
            customerName=sale.customer?.name;
            customerPhone=sale.customer?.phone;
            subtotal+=parseFloat(sale.total);
            sales.push(sale._id);
        });
        this.formGroup.patchValue(
            {
                customerPhone:customerPhone,
                customerName:customerName,
                user:currentUser._id,
                bay:currentUser.bay._id,
                total:subtotal,
                vat:0,
                subtotal:subtotal,
                sales:sales

            }
        )
    }


    async save(){
        try {
            if(!this.formGroup.valid){
                throw Error(
                    'Please provide all data required'
                )
            }
            this.loading=true;
            const _payload:InvoicePayload=this.formGroup.value;
            const response=await this.invoice.addInvoice(_payload);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.formGroup.reset();
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        message:response.message,
                        type:'success'
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        message:err.message,
                        type:'warning'
                    }
                )
            )
        }
    }




    ngOnDestroy(){this.subscription$.unsubscribe()}



}
