import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InterfaceMenusItem, menuItems } from 'src/app/models/menus.model';
import { UserModel } from 'src/app/models/users.model';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    @Select(AppState.currentUser) currentUser$:Observable<UserModel>;
    menus:InterfaceMenusItem=menuItems;

    constructor() {};

    ngOnInit(): void {};

}
