import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import api from '../constants/api';
import { WasherPayloadInterfaceModel } from '../models/washers.model';
import { UpdatePayloadIntefaceModel } from '../models/common.model';
import ResponseInterfaceModel from '../models/response.model';

@Injectable({
    providedIn: 'root'
})
export class WasherService {

    constructor (private http:HttpClient){}

    get():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/washers/get`)
                .toPromise()
                .then((res)=>res)
                .catch(err=>err);
    }

    add(data:WasherPayloadInterfaceModel):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/washers/add`,data)
                .toPromise()
                .then((res)=>res)
                .catch(err=>err);
    }

    update(data:UpdatePayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.put(`${api.user}/washers/update`,data)
                .toPromise()
                .then((res)=>res)
                .catch(err=>err);
    }
    

}