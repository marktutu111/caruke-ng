
export type bay= '_id'
    | 'name'
    | 'phone'
    | 'location'
    | 'address'
    | 'status'
    | 'createdAt'
    | 'updatedAt'


export interface BayPayloadInterface {
    email:string;
    name:string;
    phone:string;
    location:string;
    address:string;
    status:string;
}


export interface BayInterface {
    _id:string;
    email:string;
    name:string;
    phone:string;
    location:string;
    address:string;
    status:string;
    createdAt:string;
    updatedAt:string;
}


export class BayModel implements BayInterface {

    _id='';
    name='';
    phone='';
    location='';
    email='';
    address='';
    status='';
    createdAt='';
    updatedAt='';

    constructor(bay:BayInterface){
        this._id=bay._id;
        this.email=bay.email;
        this.name=bay.name;
        this.phone=bay.phone;
        this.location=bay.location;
        this.address=bay.address;
        this.status=bay.status;
        this.createdAt=bay.createdAt;
        this.updatedAt=bay.updatedAt;
    };



}