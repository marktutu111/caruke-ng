import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import ResponseInterfaceModel from '../models/response.model';
import api from '../constants/api';
import { SettingsPayload } from '../models/settings.model';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    constructor(private http:HttpClient){}

    getsetns():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/settings/get`)
                        .toPromise()
                        .then(res=>res)
                        .catch(err=>err)
    }

    savesetns(setting:SettingsPayload):Promise<ResponseInterfaceModel>{
        console.log(setting);
        return this.http.post(`${api.user}/settings/add`,setting)
                        .toPromise()
                        .then(res=>res)
                        .catch(err=>err)
    }

}