import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import api from '../constants/api';
import { PasswordUpdate, UpdatePayloadIntefaceModel } from '../models/common.model';
import ResponseInterfaceModel from '../models/response.model';
import { UsersPayloadInterface } from '../models/users.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http:HttpClient){};

    add(data:UsersPayloadInterface):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/users/add`,data)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    getall():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/users/get`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    update(data:UpdatePayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.put(`${api.user}/users/update`,data)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    updatePassword(data:PasswordUpdate):Promise<ResponseInterfaceModel>{
        return this.http.put(`${api.user}/users/password/update`,data)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    login(data:any):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/users/login`,data)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    getone(id:string):Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/users/get/${id}`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }


}