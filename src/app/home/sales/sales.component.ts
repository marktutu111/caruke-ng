import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SalesModel, SalesSummayModel } from 'src/app/models/sales.model';
import { FilterSales, GetWashers, SelectSale } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import *as moment from "moment";
import { WasherModel } from 'src/app/models/washers.model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sales',
    templateUrl: './sales.component.html',
    styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

    @Select(AppState.loading) loading$:Observable<boolean>;
    @Select(AppState.washers) washers$:Observable<WasherModel[]>;
    @Select(AppState.saleSummary) saleSummary$:Observable<SalesSummayModel>;
    @Select(AppState.sales) sales$:Observable<any[]>;

    private start=moment().startOf('month').toDate();
    private end=moment().endOf('month').toDate();
    private selected:SalesModel[]=[];

    p:number=1;
    loading:boolean=false;

    formGroup:FormGroup=new FormGroup(
        {
            washer:new FormControl(''),
            start:new FormControl(this.start,Validators.required),
            end:new FormControl(this.end,Validators.required)
        }
    )

    constructor(private store:Store,private router:Router) {};


    ngOnInit(): void {
        this.filter();
        this.store.dispatch(
            new GetWashers()
        )
    };


    refresh(){
        this.filter();
    }


    filter(){
        const form=this.formGroup;
        if(!form.valid){
            return;
        };
        this.store.dispatch(
            new FilterSales(
                form.value
            )
        )
    }


    selectSale(sale:SalesModel,e:any){
        try {
            let sales:SalesModel[]=this.selected;
            switch (e.checked) {
                case true:
                    sales.push(sale);
                    break;
                default:
                    sales=sales.filter(_sale=>_sale._id !== sale._id);
                    break;
            }
            this.selected=sales;
        } catch (err) {

        }
    }


    generateInvoice(){
        try {
            if(this.selected.length<=0)return;
            this.store.dispatch(
                new SelectSale(
                    this.selected
                )
            );
            this.router.navigate(
                [
                    'app/newinvoice'
                ]
            );
        } catch (err) {
            
        }
    }


    edit(sale:SalesModel){
        this.store.dispatch(
            new SelectSale(
                new Array(sale)
            )
        );
        this.router.navigate(
            [
                'app/editsale'
            ]
        );
    }


    getActive(sale:SalesModel){
        try {
            const sales:SalesModel[]=this.store.selectSnapshot(AppState.selectedSale);
            const _sale=sales.find(_sale=>_sale._id===sale._id);
            return _sale && _sale._id;
        } catch (err) {
            return false;
        }
    }


}
