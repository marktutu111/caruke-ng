import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BayInterface, BayModel, BayPayloadInterface } from 'src/app/models/bay.model';
import ResponseInterfaceModel from 'src/app/models/response.model';
import { AdminService } from 'src/app/services/admin.service';
import { GetBays, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-bay',
    templateUrl: './bay.component.html',
    styleUrls: ['./bay.component.scss']
})
export class BayComponent implements OnInit {

    @Select(AppState.bays) bays$:Observable<BayModel[]>;
    @Select(AppState.loading) loading$:Observable<boolean>;

    formGroup:FormGroup=new FormGroup(
        {
            _id:new FormControl(''),
            name:new FormControl('',Validators.required),
            email:new FormControl('',Validators.required),
            phone:new FormControl('',Validators.required),
            location:new FormControl('',Validators.required),
            address:new FormControl('',Validators.required),
            status:new FormControl('',Validators.required)
        }
    )

    toggle:boolean=false;
    onEdit:boolean=false;
    loading:boolean=false;
    p:number=1;

    constructor(private store:Store,private service:AdminService) {};


    ngOnInit(): void {
        this.store.dispatch(
            new GetBays()
        )
    }

    async save(){
        try {
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            const form=this.formGroup;
            if(!form.valid){
                throw Error(
                    'Invalid form'
                )
            }
            this.loading=true;
            let response:ResponseInterfaceModel;
            const data:BayInterface=form.value;
            const payload:BayPayloadInterface={
                email:data.email,
                address:data.address,
                location:data.location,
                name:data.name,
                phone:data.phone,
                status:data.status
            }
            switch (this.onEdit) {
                case true:
                    response=await this.service.updateBay(
                        {
                            id:data._id,
                            data:payload
                        }
                    );
                    break;
                default:
                    response=await this.service.addbay(payload)
                    break;
            }
            this.loading=false;
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            type:'success',
                            message:response.message,
                            show:true
                        }
                    ),
                    new GetBays()
                ]
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        type:'danger',
                        message:err.message,
                        show:true
                    }
                )
            )
        }
    }


    edit(bay:BayModel){
        this.formGroup.patchValue(
            bay
        )
    }

    
}
