import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { UsersInterfaceModel } from 'src/app/models/users.model';
import { GetSummary } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    @Select(AppState.currentUser) currentUser$:Observable<UsersInterfaceModel>;
    @Select(AppState.summary) summary$:Observable<any>;
    constructor(private store:Store) {};

    ngOnInit(): void {
        this.refresh();
    }

    refresh(){
        this.store.dispatch(
            new GetSummary()
        )
    }

}
