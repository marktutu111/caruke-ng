import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import api from '../constants/api';
import { BayPayloadInterface } from '../models/bay.model';
import { UpdatePayloadIntefaceModel } from '../models/common.model';
import ResponseInterfaceModel from '../models/response.model';
import { SubscriptionIssueInteface, SubscriptionPayloadInterface } from '../models/subscription.model';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    constructor(private http:HttpClient){};

    sendOtp(data:any):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/otp/sendotp`,data)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    subscribe(data:SubscriptionPayloadInterface):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/subscription/new`,data)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    issue(data:SubscriptionIssueInteface):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/subscription/issue`,data)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    getsubs():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/subscription/get`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    getBaysubs(bay:string):Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/subscription/bay/get/${bay}`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    addbay(data:BayPayloadInterface):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/bay/add`,data)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    updateBay(data:UpdatePayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.put(`${api.user}/bay/update`,data)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }

    getbays():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/bay/get`)
                .toPromise()
                .then(res=>res)
                .catch(err=>err);
    }
    

    
}