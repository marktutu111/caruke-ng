import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { UserModel } from './models/users.model';
import { SetCurrentUser } from './state/app.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title='ng-caruke';

  constructor(private store:Store,private router:Router){}

  ngOnInit(){
    this.getUser();
  }


  getUser(){
    try {
      const user=localStorage.getItem('caruke-auth');
      if(typeof user==='string') {
        const _user:UserModel=new UserModel(JSON.parse(user));
        this.store.dispatch(
          new SetCurrentUser(
            _user
          )
        )
        switch (_user.account) {
            case 'bay':
                this.router.navigate(['app/dashboard']);
                break;
            default:
                this.router.navigate(['app/users']);
                break;
        }
      }
    } catch (err) {
      
    } 
  }


}
