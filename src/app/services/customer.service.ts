import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import ResponseInterfaceModel from '../models/response.model';
import api from '../constants/api';
import { CustomerPayloadInterfaceModel } from '../models/customer.model';
import { UpdatePayloadIntefaceModel } from '../models/common.model';

@Injectable({
    providedIn: 'root'
})
export class CustomerService {

    constructor(private http:HttpClient){};

    getcustomers():Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/customers/get`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    searchCustomer(search:string):Promise<ResponseInterfaceModel>{
        return this.http.get(`${api.user}/customers/get?search=${search}`)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }

    update(data:UpdatePayloadIntefaceModel):Promise<ResponseInterfaceModel>{
        return this.http.put(`${api.user}/customers/update`,data)
                .toPromise()
                .then((res)=>res)
                .catch(err=>err);
    }

    addcustomer(data:CustomerPayloadInterfaceModel):Promise<ResponseInterfaceModel>{
        return this.http.post(`${api.user}/customers/add`,data)
                    .toPromise()
                    .then(res=>res)
                    .catch(err=>err)
    }


}