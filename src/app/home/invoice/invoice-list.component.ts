import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InvoiceModel } from 'src/app/models/invoice.model';
import { GetInvoices, SelectInvoice } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-invoice-list',
    templateUrl: './invoice-list.component.html',
    styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

    @Select(AppState.loading) loading$:Observable<boolean>;
    @Select(AppState.invoices) invoices$:Observable<InvoiceModel[]>;

    p:number=1;

    constructor(private store:Store,private router:Router) {};


    ngOnInit(): void {
        this.store.dispatch(
            new GetInvoices()
        )
    };


    view(inv:InvoiceModel){
        this.store.dispatch(
            new SelectInvoice(
                inv
            )
        );
        this.router.navigate(['app/invoice'])
    }

}
