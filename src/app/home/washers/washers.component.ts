import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import ResponseInterfaceModel from 'src/app/models/response.model';
import { WasherInterfaceModel, WasherModel, WasherPayloadInterfaceModel } from 'src/app/models/washers.model';
import { WasherService } from 'src/app/services/washers.service';
import { GetWashers, ShowAlert } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
    selector: 'app-washers',
    templateUrl: './washers.component.html',
    styleUrls: ['./washers.component.scss']
})
export class WashersComponent implements OnInit {

    @Select(AppState.washers) washers$:Observable<WasherModel[]>;
    loading:boolean=false;
    onedit:boolean=false;
    p:number=1;

    formGroup:FormGroup=new FormGroup(
        {
            _id:new FormControl(''),
            fname:new FormControl('',Validators.required),
            lname:new FormControl('',Validators.required),
            phone:new FormControl('',[Validators.required,Validators.maxLength(10)]),
            location:new FormControl('',Validators.required),
            gender:new FormControl('',Validators.required),
            status:new FormControl('active',Validators.required)  
        }
    )


    constructor(private service:WasherService,private store:Store) {};


    ngOnInit(): void {
        this.store.dispatch(
            new GetWashers()
        )
    };



    async save(){
        try {
            if(!this.formGroup.valid){
                throw Error(
                    'Please fill the form and  provide all the required data'
                )
            };
            this.loading=true;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            let response:ResponseInterfaceModel;
            const data:WasherInterfaceModel=this.formGroup.value;
            const payload:WasherPayloadInterfaceModel={
                fname:data.fname,
                lname:data.lname,
                phone:data.phone,
                location:data.location,
                gender:data.gender,
                status:data.status
            }
            switch (this.onedit) {
                case true:
                    response=await this.service.update(
                        {
                            id:data._id,
                            data:payload
                        }
                    );
                    break;
                default:
                    response=await this.service.add(payload);
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.onedit=false;
            this.formGroup.reset();
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            show:true,
                            message:response.message,
                            type:'success'
                        }
                    ),
                    new GetWashers()
                ]
            );
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


}
