
export interface UpdatePayloadIntefaceModel {
    id:string;
    data:object;
}


export interface PasswordUpdate {
    id:string;
    oldpassword:string;
    newpassword:string;
}