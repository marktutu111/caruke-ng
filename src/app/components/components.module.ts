import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { TabsComponent } from './tabs/tabs.component';

@NgModule({
    declarations: [
        SidebarComponent,
        NavbarComponent,
        TabsComponent
    ],
    imports: [ RouterModule,CommonModule ],
    exports: [
        SidebarComponent,
        NavbarComponent,
        TabsComponent
    ],
    providers: [],
})
export class ComponentsModule {}