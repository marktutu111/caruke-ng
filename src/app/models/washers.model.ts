

export interface WasherPayloadInterfaceModel {
    fname:string;
    lname:string;
    phone:string;
    location:string;
    gender:string;
    status:string;
}

export interface WasherInterfaceModel {
    _id:string;
    fname:string;
    lname:string;
    phone:string;
    location:string;
    gender:string;
    status:string;
    createdAt:string;
    updatedAt:string;
}


export class WasherModel implements WasherInterfaceModel {

    _id='';
    fname=''
    lname=''
    phone='';
    location=''
    gender='';
    status='';
    createdAt='';
    updatedAt='';

    checked:boolean=false;

    constructor(washer:WasherInterfaceModel){
        this._id=washer._id;
        this.fname=washer.fname;
        this.lname=washer.lname;
        this.phone=washer.phone;
        this.location=washer.location;
        this.gender=washer.gender;
        this.status=washer.status || '';
        this.createdAt=washer.createdAt || '';
        this.updatedAt=washer.updatedAt || '';
    }

    getname(){
        return `${this.fname} ${this.lname}`;
    }

    onCheck(v?:any){
        this.checked=v.checked;
    }

}