import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { PasswordUpdate } from '../models/common.model';
import ResponseInterfaceModel from '../models/response.model';
import { Settings } from '../models/settings.model';
import { UserModel, UsersPayloadInterface } from '../models/users.model';
import { SettingsService } from '../services/settings.service';
import { UserService } from '../services/users.service';
import { GetSettings, Logout, ShowAlert } from '../state/app.actions';

@Injectable({
    providedIn: 'root'
})
export class UserController {

    loading:boolean=false;
    onEdit:boolean=false;
    toggle:boolean=false;
    formGroup:FormGroup=this.fb.group(
        {
            settings:new FormGroup(
                {
                    _id:new FormControl(null),
                    sms_message:new FormControl('',Validators.required),
                    sms_sales_toggle:new FormControl(false,Validators.required)
                }
            ),
            profile:new FormGroup(
                {
                    _id:new FormControl(null),
                    fname:new FormControl('',Validators.required),
                    lname:new FormControl('',Validators.required),
                    email:new FormControl('',Validators.required),
                    password:new FormControl('',Validators.required),
                    phone:new FormControl('',Validators.required),
                    gender:new FormControl('',Validators.required),
                    role:new FormControl([],Validators.required),
                    status:new FormControl('',Validators.required)
                }
            ),
            password:new FormGroup(
                {
                    id:new FormControl('',Validators.required),
                    oldpassword:new FormControl('',Validators.required),
                    newpassword:new FormControl('',Validators.required),
                    confirm:new FormControl('',Validators.required)
                }
            )
        }
    )

    constructor(
        private store:Store,
        private fb:FormBuilder,
        private service:UserService,
        private settingsService:SettingsService
    ) {};


    async save(){
        try {
            if(!this.formGroup.get('profile')){
                throw Error(
                    'Please provide all data'
                )
            };
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            )
            this.loading=true;
            let response:ResponseInterfaceModel;
            const data:UserModel=this.formGroup.get('profile')?.value;
            const _payload:UsersPayloadInterface={
                email:data.email,
                gender:data.gender,
                fname:data.fname,
                lname:data.lname,
                password:data.password,
                phone:data.phone,
                role:data.role,
                status:data.status
            }
            switch (this.onEdit) {
                case true:
                    response=await this.service.update(
                        {
                            id:data._id,
                            data:_payload
                        }
                    );
                    break;
                default:
                    response=await this.service.add(_payload);
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.formGroup.reset();
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:'User created successfully',
                        type:'success'
                    }
                )
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


    onChecked(_role:any,e:any){
        let {role}=this.formGroup.get('profile')?.value;
        switch (e.checked) {
            case true:
                role=[...role,_role];
                break;
            default:
                role=role.filter((r:string)=>r!==_role);
                break;
        }
        this.formGroup.get('profile')?.patchValue(
            {
                role:role
            }
        )
    }


    checked(role:any){
        return this.formGroup.get('profile')?.get('role')?.value.indexOf(role)>-1;
    }



    edit(user:UserModel){
        this.toggle=true;
        this.onEdit=true;
        this.formGroup.get('profile')?.patchValue(
            user
        )
    }



    async saveSettings(){
        try {
            if(!this.formGroup.get('settings')?.valid){
                throw Error(
                    'invalid form'
                )
            }
            this.loading=true;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            );
            const form:Settings=this.formGroup.get('settings')?.value;
            const response=await this.settingsService.savesetns(
                {
                    sms_message:form.sms_message,
                    sms_sales_toggle:form.sms_sales_toggle
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            show:true,
                            message:response.message,
                            type:'success'
                        }
                    ),
                    new GetSettings()
                ]
            )
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }


    async updatePassword(){
        try {
            const form=this.formGroup.get('password');
            if(!form?.valid){
                throw Error(
                    'invalid form'
                )
            };
            const {oldpassword,newpassword,confirm,id}=form.value;
            if(newpassword !== confirm){
                throw Error(
                    'Password does not match'
                )
            };
            this.loading=true;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:false
                    }
                )
            );
            const _payload:PasswordUpdate={
                id:id,
                newpassword:newpassword,
                oldpassword:oldpassword
            }
            const response=await this.service.updatePassword(_payload);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.loading=false;
            this.store.dispatch(
                [
                    new ShowAlert(
                        {
                            show:true,
                            message:response.message,
                            type:'success'
                        }
                    ),
                    new Logout()
                ]
            );
        } catch (err:any) {
            this.loading=false;
            this.store.dispatch(
                new ShowAlert(
                    {
                        show:true,
                        message:err.message,
                        type:'danger'
                    }
                )
            )
        }
    }



}